-- MySQL Workbench Forward Engineering


CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;


CREATE TABLE IF NOT EXISTS `mydb`.`CadastroPets` (
  `idCadastroPets` INT NOT NULL,
  `nomePet` VARCHAR(45) NOT NULL,
  `raça` CHAR NOT NULL,
  `cor` CHAR NOT NULL,
  `descricaoPet` VARCHAR(255) NOT NULL,
  `idade` INT NOT NULL,
  PRIMARY KEY (`idCadastroPets`));
  
  
CREATE TABLE IF NOT EXISTS `mydb`.`Ongs` (
  `idOngs` INT NOT NULL,
  `nomeOng` VARCHAR(80) NOT NULL,
  `endereco` VARCHAR(100) NOT NULL,
  `telefone` CHAR(16) NOT NULL,
  `cnpjDaOng` INT NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `responsavel` VARCHAR(50) NOT NULL,
  `CadastroPets_idCadastroPets` INT NOT NULL,
  PRIMARY KEY (`idOngs`),
  INDEX `fk_Ongs_CadastroPets1_idx` (`CadastroPets_idCadastroPets` ASC),
  CONSTRAINT `fk_Ongs_CadastroPets1`
    FOREIGN KEY (`CadastroPets_idCadastroPets`)
    REFERENCES `mydb`.`CadastroPets` (`idCadastroPets`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
CREATE TABLE IF NOT EXISTS `mydb`.`voluntario` (
  `idvoluntario` INT NOT NULL,
  `nomevoluntario` VARCHAR(80) NOT NULL,
  `endereco` VARCHAR(100) NOT NULL,
  `telefone` CHAR(16) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `cpf` INT NOT NULL,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idvoluntario`));
  
  
CREATE TABLE IF NOT EXISTS `mydb`.`tarefasVoluntario` (
  `idtarefasVoluntario` INT NOT NULL,
  `nomeVoluntario` VARCHAR(80) NULL,
  `pertenceOng` VARCHAR(45) NULL,
  `funcao` VARCHAR(50) NULL,
  `comentario` VARCHAR(255) NULL,
  `dataExecucao` DATETIME NULL,
  `Ongs_idOngs` INT NOT NULL,
  PRIMARY KEY (`idtarefasVoluntario`),
  INDEX `fk_tarefasVoluntario_Ongs1_idx` (`Ongs_idOngs` ASC),
  CONSTRAINT `fk_tarefasVoluntario_Ongs1`
    FOREIGN KEY (`Ongs_idOngs`)
    REFERENCES `mydb`.`Ongs` (`idOngs`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    
CREATE TABLE IF NOT EXISTS `mydb`.`voluntario_has_tarefasVoluntario` (
  `voluntario_idvoluntario` INT NOT NULL,
  `tarefasVoluntario_idtarefasVoluntario` INT NOT NULL,
  `dataCadastro` VARCHAR(45) NULL,
  PRIMARY KEY (`voluntario_idvoluntario`, `tarefasVoluntario_idtarefasVoluntario`),
  INDEX `fk_voluntario_has_tarefasVoluntario_tarefasVoluntario1_idx` (`tarefasVoluntario_idtarefasVoluntario` ASC),
  INDEX `fk_voluntario_has_tarefasVoluntario_voluntario1_idx` (`voluntario_idvoluntario` ASC),
  CONSTRAINT `fk_voluntario_has_tarefasVoluntario_voluntario1`
    FOREIGN KEY (`voluntario_idvoluntario`)
    REFERENCES `mydb`.`voluntario` (`idvoluntario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_voluntario_has_tarefasVoluntario_tarefasVoluntario1`
    FOREIGN KEY (`tarefasVoluntario_idtarefasVoluntario`)
    REFERENCES `mydb`.`tarefasVoluntario` (`idtarefasVoluntario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    
CREATE TABLE IF NOT EXISTS `mydb`.`artigos` (
  `idartigos` INT NOT NULL,
  `titulo` VARCHAR(45) NOT NULL,
  `texto` TEXT NOT NULL,
  `imgPrincipal` BINARY NOT NULL,
  `imgSegundaria` BINARY NOT NULL,
  PRIMARY KEY (`idartigos`));


CREATE TABLE IF NOT EXISTS `mydb`.`parceiros` (
  `idparceiros` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `imgPrincipal` BINARY NOT NULL,
  `imgSegundaria` BINARY NOT NULL,
  PRIMARY KEY (`idparceiros`));


CREATE TABLE IF NOT EXISTS `mydb`.`doacao` (
  `iddoacao` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `imgPrincipal` BINARY NOT NULL,
  `imgSegundaria` BINARY NOT NULL,
  PRIMARY KEY (`iddoacao`));


CREATE TABLE IF NOT EXISTS `mydb`.`videoDia` (
  `idvideoDia` INT NOT NULL,
  `titulo` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(355) NOT NULL,
  `video` VARCHAR(70) NOT NULL,
  PRIMARY KEY (`idvideoDia`));


CREATE TABLE IF NOT EXISTS `mydb`.`videoDoados` (
  `idvideoDoados` INT NOT NULL,
  `titulo` VARCHAR(45) NULL,
  `video` VARCHAR(70) NULL,
  `descricao` VARCHAR(355) NULL,
  PRIMARY KEY (`idvideoDoados`));


CREATE TABLE IF NOT EXISTS `mydb`.`faq` (
  `idfaq` INT NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `telefone` CHAR NOT NULL,
  `texto` TEXT NOT NULL,
  PRIMARY KEY (`idfaq`));
